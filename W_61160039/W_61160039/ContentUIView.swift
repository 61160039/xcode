//
//  ContentUIView.swift
//  W_61160039
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct ContentUIView: View {
    var body: some View {
        List(/*@START_MENU_TOKEN@*/0 ..< 5/*@END_MENU_TOKEN@*/) { item in
            ContentView()
            
        }.listStyle(InsetGroupedListStyle())
        
        List(/*@START_MENU_TOKEN@*/0 ..< 5/*@END_MENU_TOKEN@*/) { item in
            TWUIView()
        }
    }
}

struct ContentUIView_Previews: PreviewProvider {
    static var previews: some View {
        ContentUIView()
    }
}
