//
//  W_61160039App.swift
//  W_61160039
//
//  Created by student on 12/18/20.
//

import SwiftUI

@main
struct W_61160039App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
