//
//  TWUIView.swift
//  W_61160039
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct TWUIView: View {
    var body: some View {
        HStack(alignment: .top){
            Image(systemName: "applelogo")
                .renderingMode(.template).frame(width:50, height: 60, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).imageScale(.large)
            VStack(alignment: .leading){
                HStack{
                    Text("Apple")
                    Image(systemName: "checkmark.circle.fill").foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    Text("@Apple")
                        .foregroundColor(Color.gray)
                    Spacer()
                    Image(systemName: "ellipsis")
                        .padding(.trailing,-30)
                        .foregroundColor(.secondary)
                }
                Text("นำ iPhone ของคุณมาแลก แล้วอัพเกรดเป็น iPhone 12 mini เครื่องใหม่ ")
                Image("A14B12").resizable(capInsets: /*@START_MENU_TOKEN@*/EdgeInsets()/*@END_MENU_TOKEN@*/, resizingMode: /*@START_MENU_TOKEN@*/.stretch/*@END_MENU_TOKEN@*/)
                    .frame(width: 300, height: 300, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                Text("ขอแนะนำ iPhone 12 mini")
                Text("apple.com")
                    .foregroundColor(Color.gray)
                HStack{
                    Image(systemName: "message").foregroundColor(.secondary)
                    Text("1").padding(.trailing,30).foregroundColor(.secondary)
                    Spacer()
                    Image(systemName: "arrow.2.squarepath").foregroundColor(.secondary)
                    Text("11").padding(.trailing,30).foregroundColor(.secondary)
                    Spacer()
                    Image(systemName: "heart").foregroundColor(.secondary)
                    Text("18").padding(.trailing,30).foregroundColor(.secondary)
                    Spacer()
                    Image(systemName: "square.and.arrow.up").padding(.trailing,30).foregroundColor(.secondary)
                    

                }
                .padding(.top)
                HStack{
                Image(systemName: "arrow.up.right.square.fill").foregroundColor(/*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                    Text("Promoted")
                        .foregroundColor(Color.gray)
                }
                
            }.padding([.top, .bottom, .trailing])
        }
           }
}

struct TWUIView_Previews: PreviewProvider {
    static var previews: some View {
        TWUIView()
    }
}
