//
//  ContentView.swift
//  W_61160039
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HStack(alignment: .center){
            Image("mango-smoothie-9").resizable(capInsets: /*@START_MENU_TOKEN@*/EdgeInsets()/*@END_MENU_TOKEN@*/, resizingMode: .stretch)
                .frame(width: 100.0, height: 150.0)
            VStack(alignment: .leading){
                Text("Mango Smoothie")
                    .fontWeight(.bold)
                Text("Mango banana,Almond milk")
                Text("140 Calories")
                    .foregroundColor(Color.gray)
                HStack{
                    Image(systemName: "star.fill")
                    Image(systemName: "star.fill")
                    Image(systemName: "star.fill")
                    Image(systemName: "star.fill")
                    Image(systemName: "star.fill")
                }.foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                .padding(.top)
            }
            
        }
        .padding()
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
